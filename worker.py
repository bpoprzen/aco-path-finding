from Graph import Graph
from Node import Node
from Edge import Edge
from Ant import Ant
import re
import random
from operator import itemgetter


class Worker:

    def __init__(self, number_of_ants, iterations):
        self.ant_num = number_of_ants
        self.iter_max = iterations
        self.evap_fac = 0.1  # evaporation factor
        self.pheromone_multiplier = pow(10, 7)  # multiplier for increasing pheromone on successful paths
        self.init_id = None
        self.dest_id = None
        self.graph = None
        self.ants = None
        self.best_path_len = pow(2, 30)
        self.best_path = []
        self.parser = re
        self.parser_format = self.parser.compile('[(,):\n]')

        self.local_evap_fac = 0.2  # every time an edge is chosen, it is deprived of some pheromone in this iteration
        # used so that uccessive ants don't choose the same nodes
        self.edge_stats = {}  # key - edge
        # value - number of times this edge has been locally deprived off pheromone
        # used so that we can return pheromone to these edges after an iteration ends

    # +-----------------------------------------------------------------------+

    def _parse_line(self, line):
        """
            Parsira red u fajlu u cvorove grafa
        """
        splited = self.parser.split(self.parser_format, line)

        # ------------------------------------------------------------
        node = Node(splited[0], float(splited[1]), float(splited[2]))
        # ------------------------------------------------------------

        # only add nodes which are fully specified in the file
        self.graph.nodes[node._id] = node

        # with each node, update graph coordinates
        self.graph.update_extreme_cords(node)

        neighbour_keys = [splited[i] for i in range(4, len(splited) - 1)]
        for i, identity in enumerate(neighbour_keys):

            # ------------------------------------------------------------
            neighbour = Node(identity, None, None)
            # ------------------------------------------------------------

            # only add edges if both of the nodes(node and neighbour) have been fully specified
            # node was fully specified at the beginning of this function
            if identity in self.graph.nodes:
                # neighbour also fully specified
                neighbour = self.graph.nodes[neighbour._id]
                e = Edge(neighbour, node)
                e._calculate_distance()
                # e._calculate_weight(self.graph.max_distance)
                self.graph.update_max_edge(e)
                self.graph.add_edge(e)

    def generate_graph(self):
        """
        Generates the initial graph
        """

        # +-------------------------------------------------------------------+
        # 1. Read all the nodes and create all the edges
        # +-------------------------------------------------------------------+

        f = open("data_path_nodes.txt", 'r')
        self.graph = Graph()
        while True:
            node_txt = f.readline()
            if node_txt is None or node_txt == "":
                break
            self._parse_line(node_txt)

    def set_ids(self, s_id, d_id):
        self.init_id = s_id
        self.dest_id = d_id

    def create_colony(self):
        if self.ants is None:
            "Ako je ovo prvi prolaz, kreiraj mrave"
            self.ants = [Ant(self.graph.nodes[self.init_id]) for i in range(self.ant_num)]
        else:
            "Ako je drugi prolaz, samo osvezi svakog mrava"
            for ant in self.ants:
                ant.__init__(self.graph.nodes[self.init_id])

    """
    def _addStartingPoint(self):
        "Adds self.dest_id as the starting point of every ant path"
        for ant in self.ants:
            ant.path_covered.append(self.init_id)
    """

    def _decideNext(self, ant):
        # take the current node on which this ant resides
        node = ant.current_node

        # +-------------------------------------------------------------------+
        # Calculate the sum of pheromone levels of incident edges (needed for
        # the scores formulae)
        # +-------------------------------------------------------------------+
        totalCost = node.calculate_total_cost()

        # +-------------------------------------------------------------------+
        # Calculate the scores of incident edges
        # +-------------------------------------------------------------------+
        scores = {edge: random.random() * edge._pheromone / float(totalCost)
                  for edge in node._incident_edges}

        # +-------------------------------------------------------------------+
        # Get the best ranked edge from node
        # +-------------------------------------------------------------------+  
        if len(scores) != 1 and len(ant.path_covered) != 0:
            """IMPORTANT: Delete the edge this ant came from"""
            del scores[ant.path_covered[len(ant.path_covered) - 1]]

        best_edge = max(scores, key=lambda x: scores[x])

        if best_edge._pheromone > 5:  # don't allow pheromone to fall under a fixed constant
            best_edge._pheromone *= (1 - self.local_evap_fac)
            if best_edge in self.edge_stats:
                self.edge_stats[best_edge] += 1 #remember to return pheromone
            else:
                self.edge_stats[best_edge] = 1 #remember to return pheromone

        ant.path_covered.append(best_edge)  # add the best edge to ants history
        ant.path_length += best_edge._distance  # add edge distance to total path cost
        ant.current_node = best_edge._get_other_node(
            node)  # set the incident edge of node through best_edge as ants' current node

    def searchGraph(self):
        pathFound = False
        successfulAnts = []
        counter = 0
        while not pathFound or counter < 1000:
            for ant in self.ants:
                if ant.current_node._id == self.dest_id:
                    continue  # if this ant has already found its path then no need to search anything
                self._decideNext(ant) #make your next move
                # path found?
                if ant.current_node._id == self.dest_id:
                    pathFound = True
                    if ant.path_length < self.best_path_len: #if this is the best solution globally, remember it
                        self.best_path_len = ant.path_length
                        self.best_path = ant.path_covered
                    successfulAnts.append(ant)
            counter += 1
        return successfulAnts

    def return_weights(self):
        for edge in self.edge_stats:
            edge._pheromone /= pow(1 - self.local_evap_fac, self.edge_stats[edge])
        self.edge_stats = {}

    def update_weights(self, successful_ants):
        temp_set = set()  # store edges that were on the successful path

        # +-------------------------------------------------------------------+
        # Find the best ant
        # +-------------------------------------------------------------------+

        path_scores = [ant.path_length for ant in successful_ants]
        min_item = min(enumerate(path_scores), key=itemgetter(1))
        best_ant = successful_ants[min_item[0]]

        # +-------------------------------------------------------------------+
        # Update the pheromone for the BEST PATH's edges
        # +-------------------------------------------------------------------+

        for edge in best_ant.path_covered:
            if edge not in temp_set:
                edge._update_pheromone(best_ant.path_length,
                                       self.pheromone_multiplier,
                                       0) #zero means zero aas evaporation factor
                # edge._calculate_weight(self.graph.max_distance)
            temp_set.add(edge)

        # +-------------------------------------------------------------------+
        # Update the pheromone for the SUCCESSFUL PATHS' edges (excluding the
        # ones that are on the BEST PATH)
        # +-------------------------------------------------------------------+

        evaporation = self.evap_fac
        for ant in successful_ants:
            if ant == best_ant: continue
            for edge in ant.path_covered:
                if edge not in temp_set:
                    edge._update_pheromone(ant.path_length, self.pheromone_multiplier, evaporation)
                # edge._calculate_weight(self.graph.max_distance)
                temp_set.add(edge)

        # edges that were not on a winning path also need do be deprived off some pheromone
        for edge in self.graph._edges:
            if edge in temp_set: continue
            edge._pheromone *= (1 - self.evap_fac)

    def run(self):
        """
            1.Ucitaj cvorove iz fajla
            2.Generisi prostor pretrage(graf sa cvorovima)
            3.Postavi parametre pretrage - za sada samo pocetnu i krajnju lokaciju
            4.Kreiraj koloniju jeftinih mrava

        """
        self.generate_graph()
        "Zakucaj za pocetak pretragu na ova dva cvora"
        self.set_ids("3653296222", "3653134376")
        for i in range(self.iter_max):
            self.create_colony()
            successful_ants = self.searchGraph()
            self.return_weights()
            self.update_weights(successful_ants)

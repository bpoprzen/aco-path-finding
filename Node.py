import math
class Node:
    """
    Attrs:
        _incident_edges :   List of edges incident to this node

    """
    def __init__(self, _id, cor1, cor2):
        self._id = _id
        self.cor1 = cor1
        self.cor2 = cor2
        self._incident_edges = []

    def calculate_total_cost(self):
        """
            Calculates the sum of all costs
        """
        return sum(edge._pheromone for edge in self._incident_edges)

    def add_incident_edge(self, _edge):
        """
        Adds an incident edge
        """
        self._incident_edges.append(_edge)

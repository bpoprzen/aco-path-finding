# ACO path-finding #
Authors: <br/>
Milan Djuric, https://gitlab.com/DjukaBazuka16<br/>
Bojan Poprzen, https://gitlab.com/bpoprzen
## Project and solution description
Project description and solution description are given in the 'Aco_path_finding.ipynb' file.
## Solution visualization
Solution visualization is given in the 'Aco_path_finding.ipynb' file.
## Requirements
Install the requirements using <i>pip</i> with the following commands:
<br/>
<b>cd "dir_where_this_repo_was_cloned"</b><br/>
<b>pip install -r requirements.txt</b>

- If you're using the <i>Anaconda distribution:

Install the requirements for <i>Anaconda</i> with the following commands in the Anaconda prompt:
<br>
<b>conda install --yes --file requirements.txt <br/>
while read requirement; do conda install --yes $requirement; done < requirements.txt</b>
import math


class Graph:
    """
    Attrs:
        max_distance : 	Max distance between two nodes in the Graph.
                        Serves as a scalling value when calculating the
                        attractiveness of an edge

    """

    def __init__(self):
        self.nodes = {}
        self._edges = []
        self.max_distance = 0  # potrebno da se zna pri iscrtvavanju grafa
        self.extreme_cor1 = {'max': -1, 'min': math.pow(2, 30)}
        self.extreme_cor2 = {'max': -1, 'min': math.pow(2, 30)}

    def __len__(self):
        return len(self.nodes)

    # +-----------------------------------------------------------------------+

    def add_edge(self, edge):
        self._edges.append(edge)


    def update_extreme_cords(self, node):
        if node.cor1 < self.extreme_cor1['min']:
            self.extreme_cor1['min'] = node.cor1
        if node.cor1 > self.extreme_cor1['max']:
            self.extreme_cor1['max'] = node.cor1
        if node.cor2 < self.extreme_cor2['min']:
            self.extreme_cor2['min'] = node.cor2
        if node.cor2 > self.extreme_cor2['max']:
            self.extreme_cor2['max'] = node.cor2

    def update_max_edge(self, edge):
        if self.max_distance < edge._distance:
            self.max_distance = edge._distance

    def _generate_path_edges(self, path):
        """
        Generates a list of undirected edges an ant moved on based on the ant's
        path (list of nodes the ant visited)
        """
        path_edges = []
        node_prev = path[0]
        del self.path[0]
        for node in self.path:
            for edge in node._incident_edges:
                if edge._get_other_node() == node_prev:
                    path_edges.append(edge)
                    break
            node_prev = node

        return path_edges

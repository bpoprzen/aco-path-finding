import math

class Edge:
    """
    Attrs:
        _pheromone : 	Indicates the pheromone level -> Knowledge of whether
                        this edge was chosen as the best path

        _distance  :	Indicates the distance between the incident nodes ->
                        Resembles the heuristic of desirability of choosing an
                        edge. (The smaller the distance the bigger the chances
                        of an ant choosing the edge).
                        Distance is calculated when all the nodes are loaded.

        _weight :       Represents a desirability of choosing an edge.
                        Weight is calculated with the formula:
                        _weight = _pheromone + length_of_the_longest_edge / _distance
    """

    def __init__(self, _node1, _node2):
        self._node1 = _node1
        self._node2 = _node2
        self._node1.add_incident_edge(self)
        self._node2.add_incident_edge(self)

        self._pheromone = 300
        self._distance = 0
        #self._weight = 0

    # +---------------------------------------------------------------------------+
    # 	Utility methods

    def _eucledean_distance(self):

        return math.sqrt(
            math.pow(self._node1.cor1 - self._node2.cor1, 2) + math.pow(self._node1.cor2 - self._node2.cor2, 2))


    def _calculate_distance(self):
        self._distance = self._eucledean_distance()
        self._distance = pow(10, -5) if self._distance < pow(10, -5) else self._distance
        return self._distance

    """
    def _calculate_weight(self, longest_edge_value):
        self._weight = self._pheromone #+ longest_edge_value / self._distance
    """

    def _get_other_node(self, node):
        """
        Returns the node incident to the parameter node, so that edge object
        which this function is called on connects the returning node and the
        parameter node.

        * This function is needed because the Graph is not directed

        parameter node -------EDGE OBJECT------- returning node
        """
        if (self._node1 == node):
            return self._node2
        return self._node1

    def _update_pheromone(self, path_distance, pheromon_multiplier, evap_fac):
        #only for nodes that are on the winning path
        self._pheromone *= (1 - evap_fac)
        self._pheromone += pheromon_multiplier / path_distance
        #self._pheromone += 100
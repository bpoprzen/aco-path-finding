{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ACO path-finding algorithm with visualization #\n",
    "_Authors: Milan Djuric, Bojan Poprzen_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Problem statement and brief algorithm description\n",
    "The purpose of this project was to find the shortest path between two selected vertices in a graph using the <b><i>Ant Colony Optimization</i></b> algorithm. \n",
    "<p><b><i>Ant Colony Optimization</i></b> is a population-based algorithm used to find solutions for difficult optimization problems which require graph traversals. The algorithm is used in many problems in which applying conventional backtracking algorithms might be too much time and memory consuming. The principal idea of the algorithm is to use artificial ants in order to mimic the behaviour of real world ants and their communication pattern. Unlike other algorithms that might be used for solving shortest-path problems, ant colony optimization is of stohastic nature, in other words, it does not provide the same solution for multiple instances of the same problem.</p>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Problem description\n",
    "As stated in the previous paragraph, this is a path-finding problem. The task is to find the optimal path between two vertices in a given graph. The given graph is quite large - it contains more than 16 thousand vertices and more than 17 thousand edges. Given this fact, we cannot conclude whether the graph is connected or not. However, after visualizing the graph, one can notice that the total number of connected components for this graph is greater than one. Therefore, the graph is not connected, and there are vertices for which no path exists. We are not going to be concerned with such vertices, since there is no point of finding an optimal solution if no solution exists at all. <p>It is important to note that there are cycles in this graph. The proof of this statement is the fact that the number of edges is bigger than the number of vertices. In other words, there are vertices for which more than one path exists. Our goal is to find the best path for such vertices.</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ant Colony Optimization algorithm in general\n",
    "_Ant Colony Optimization_ uses artificial ants in order to mimic the behaviour of real world ants and their communication pattern. </br>\n",
    "Algorithm works in $N_{iter}$ iterations, where $N_{iter}$ is user-defined. One iteration is defined by three processes: <br/>\n",
    "1. Create ant colony\n",
    "2. Search the graph\n",
    "3. Update the pheromone amounts that each of the edges store\n",
    "\n",
    "#### 1. Create ant colony\n",
    "The process of creating an ant colony implies the creation of artificial ants and setting their starting position (starting vertex for graph traversals). The number of ants $N_{ants}$ is user-defined.\n",
    "\n",
    "#### 2. Search the graph\n",
    "Graph search is a process in which every ant $A_i$, where $i \\in N_{ants}$, decides which path to take to have the best chances of finding the destination vertex. The decision of which path to take boils down to choosing the _most attractive_ incident edge of a vertex, starting from the starting vertex; After visiting the chosen incident edge (_arriving_ to the other end of the edge), the whole process is then repeated until $A_i$ hasn't come to the destination vertex. </br>\n",
    "\n",
    "Ant $A_i$'s decision process at a vertex $\\upsilon$ follows: </br>  \n",
    "\n",
    "Ant is residing at the vertex $\\upsilon$. His decision of which incident edge $\\epsilon$ to visit is based on the formulae:\n",
    "<p>\n",
    "    $$\\tau(e) = \\Re * \\frac{\\phi(e)}{\\sum_{e \\in Inc(\\upsilon)} \\phi(e)},\\\\\n",
    "    \\tau(e) - \\text{attractiveness of an edge e} \\\\\n",
    "    \\Re - \\text{random number $\\in [0, 1]$} \\\\\n",
    "    \\phi(e) - \\text{amount of pheromone on the edge e}\\\\\n",
    "    Inc(\\upsilon) - \\text{incident edges of the $\\upsilon$ vertex}$$\n",
    "</p>\n",
    "<p>\n",
    "<br/>\n",
    "    $$\\tau(\\epsilon) = \\max_{e \\in Inc(\\upsilon)} \\tau(e) \\implies \\text{Ant has chosen the edge } \\epsilon$$\n",
    "</p>\n",
    "<p>\n",
    "    The process of graph search stops when an ant has come to the destination vertex. <br/>\n",
    "    The algorithm needs some kind of a <b>procedure to update the pheromone amounts on the edges in the graph. This is done in order to make the edges on successful paths more attractive in the next iteration. <br/>\n",
    "    This is also the reason why a random component $\\Re$ needs to be incorporated in the edge decision formulae - not all ants ought to choose the edges which have more pheromone. Doing so would unvary the search space, so possibly a better path wouldn't be discovered.</b>\n",
    "</p>\n",
    "\n",
    "#### 3. Update the pheromone amounts each of the edges store\n",
    "After Ants $A_i, i \\leq N_{ants}$ have come to the destination edge, the process of graph search ends.<br/>\n",
    "For every edge $e \\in Edges(Graph)$ pheromone level $\\phi(e)$ is set by the formulae:\n",
    "<p>\n",
    "$$\n",
    "\\phi_{new}(e) = \\phi_{old}(e) + \\frac{c}{length\\text{($A_k$'s path)}},\\quad \\text{if $e$ on $A_k$ ant's path, where ant $A_k$'s path is the shortest}\n",
    "$$\n",
    "</p>\n",
    "<p>\n",
    "$$\n",
    "\\phi_{new}(e) = (1 - \\xi) * \\phi_{old}(e) + \\frac{c}{length\\text{($A_j$'s path)}},\\quad \\text{if $e$ on $A_j$ ant's path, where $A_j \\in$ successful ants \\ $\\{ A_k \\}$ }\n",
    "$$\n",
    "</p>\n",
    "<p>\n",
    "$$\n",
    "\\phi_{new}(e) = (1 - \\xi) * \\phi_{old}(e), \\quad \\text{if $e$ on $A_m$ ant's path, where $A_m \\not\\in$ successful ants}\n",
    "$$\n",
    "<p>\n",
    "    $$c - \\text{A constant}\\\\\n",
    "    \\xi - \\text{Evaporation factor $\\in [0, 1]$}\\\\\n",
    "    $$\n",
    "</p>\n",
    "Important: If an edge $e$ is on multiple ant paths, then the $\\phi(e)$ will be updated only once (the first time in the following order). The order in which the pheromone levels $\\phi(e)$ are updated is - firstly the edges on the shortest path, then on the other successful paths and then the edges on the unsuccessful paths.\n",
    "<p>\n",
    "    <hr width=20% align=\"left\">\n",
    "    Implementation of the <i>Ant Colony Optimization</i> algorithm done in this project has added features to the generalized <i>ACO</i> algorithm in these parts:\n",
    "   <ul style=\"list-style-type:circle\">\n",
    "    <li><b>Ants do one step at a time</b> - One step means covering one edge. Idea of the feature is to give the opportunity for multiple ants to complete their paths at the same step.</li>\n",
    "    <li><b>Local pheromone reduction</b> - reducing pheromone levels of an edge by a certain percentage every time an ant choses it as its next move. Intention with this feature is to <i>mutate</i> the search space.</li>\n",
    "    </ol>\n",
    "    These features and the reasons for their implementation are explained in detail in the \"Implementation\" section.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implementation\n",
    "### Graph search\n",
    "In each algorithm iteration, a graph search is performed. This search always begins from the starting vertex, and does not end until at least one ant has reached the destination vertex and until all ants have completed a fixed number of steps. One step for an artificial ant means covering one edge.<div>The main reason why we introduced the requirement for every ant to complete a certain number of steps before ending the traversal is to increase the number of successful ants and completed tours in one algorithm iteration. The principal idea was to have more than one successful ant in each iteration, in order for the upcoming pheromone update to be more meaningful.</div>\n",
    "![title](images/20.png)\n",
    "<div>The important thing to notice in the code is that we update the global best solution if the newest solution is better. This is one of the main ideas of swarm intelligence algorithms in general - work with multiple instances of a solution that converge to the best solution as the number of iterations converges to infinity.</div>Bare in mind that the shortest path is the one for which the sum of all edge distances is the smallest, and that does not necessarilly imply the smallest number of visited vertices.\n",
    "<div></div>\n",
    "\n",
    "### Local pheromone reduction and local pheromone retrieval\n",
    "\n",
    "One of the problems we faced during the development was the fact that, during one iteration, successive ants which reside on the same vertex tend to opt for the same edge as their next move. Such behaviour is perfectly explainable and predictable. If one ant opts to move across certain edge, there is a high chance that this edge has more pheromone than any other edge incident to the same vertex. In other words, the edge is the most appealing for this ant, and the odds are it will also be deemed the most appealing by successive ants. Such behaviour significantly impairs the search process, as our goal is to diversify the search, and not to send all ants in the same direction. Moreover, such behaviour is extremely prevalent in early iterations, when all ants tend to reside on the same vertices.<div>The mechanism we used for handling this issue was <b><i>local pheromone reduction</i></b>. The idea was to reduce edge pheromone by a certain percentage every time an ant choses it as its next move.This might be counterintuitive at first glance, since it is completely opposed to what happens with real life ants.</div>\n",
    "![title](images/21.png)\n",
    "<div>It is important to bare in mind that this reduction is of local nature, in other words, after each iteration we return the entire amount of pheromone we have taken during the iteration. We achieve this by memorising how many times each edge has been traversed during one iteration.</div>\n",
    "\n",
    "![title](images/22.png)\n",
    "\n",
    "### Pheromone update\n",
    "*Initial pheromone values are set to a constant.* <br/>\n",
    "Pheromone update ensures that the algorithm converges towards an optimal solution. The update is executed after each iteration. In general ACO algorithms, update consists of two steps.\n",
    "<div><br/><b>1. Increase pheromone on edges that are part of a successful path <br/>2. Decrease pheromone on each edge as a part of the ususal pheromone evaporation</b>\n",
    "</div>\n",
    "<br/>\n",
    "In our implementation we decided to use a variable approach for each edge, depending on how utilized a certain edge was in successful tours. For instance, edges that are part of the most successful tour undergo only pheromone increase, as we want to render these edges more appealing in next iterations. Consequently, performing pheromone evaporation on these edges would be counterintuitive.\n",
    "\n",
    "![title](images/23.png)\n",
    "\n",
    "On the other hand, edges that are part of a successful tour that is not considered as the best in this iteration face both pheromone increase and pheromone evaporation. We achieve this by passing the actual evaporation factor to the function responsible for updating pheromone(rather than a zero like in the previous case).\n",
    "\n",
    "![title](images/24.png)\n",
    "\n",
    "Ultimately, edges that are not part of any path that was found between the starting and the destination vertex undergo only pheromone evaporation.\n",
    "\n",
    "![title](images/25.png)\n",
    "\n",
    "Of course, we also maintain a set that ensures that each edge faces pheromone update only once, as some edges may be part of multiple successful paths."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "_Ant Colony Optimization_ algorithm has proven to be efficient for solving the given problem. Furthermore, the added implementation features helped the convergence of the algorithm. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Notes\n",
    "Visualization of the given graph and afterwards the best path is not for commercial purposes. Intentions for the creation of it was to test the implemented algorithm. The bqplot and ipywidgets libraries were not designed to support graphs of this size efficiently. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualize the data points and get the two chosen data points ##"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Choose algorithm parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Choose the number of ants and iterations for the ACO algorithm:\n",
      "    *Default values have proven to be appropriate for the generic case.\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "ee0a445823e74794b489688216ac3c37",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "IntSlider(value=20, description='Ants: ', min=5)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "cf3f177202e34865a302bdd03a3024ed",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "IntSlider(value=100, description='Iterations: ', max=200, min=20)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import numpy as np\n",
    "from bqplot.marks import Graph\n",
    "from bqplot.toolbar import Toolbar\n",
    "from bqplot import LinearScale, Figure\n",
    "from ipywidgets import Layout, IntSlider\n",
    "from main import main\n",
    "from worker import Worker\n",
    "from IPython.display import display\n",
    "\n",
    "fig_layout = Layout(width='1000px', height='600px')\n",
    "\n",
    "ant_slider = IntSlider(min=5,max=100,step=1,value=20,description=\"Ants: \")\n",
    "iterations_slider = IntSlider(min=20,max=200,step=1,value=100,description=\"Iterations: \")\n",
    "print(\"Choose the number of ants and iterations for the ACO algorithm:\")\n",
    "print(\"    *Default values have proven to be appropriate for the generic case.\")\n",
    "display(ant_slider, iterations_slider)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Initialize worker and generate graph model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "worker = Worker(ant_slider.value, iterations_slider.value)\n",
    "worker.generate_graph()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Generate graph view model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "link_data = []\n",
    "node_data = []\n",
    "x = []\n",
    "y = []\n",
    "\n",
    "node_id_list = list(worker.graph.nodes.keys())\n",
    "node_id_index = {}\n",
    "for index, node_id in enumerate(node_id_list):\n",
    "    node_data.append({'shape': 'circle', 'label_display': 'none', 'shape_attrs': {'r': 2}, 'id': node_id})\n",
    "    x.append(worker.graph.nodes[node_id].cor1)\n",
    "    y.append(worker.graph.nodes[node_id].cor2)\n",
    "    \n",
    "    node_id_index[node_id] = index\n",
    "    \n",
    "for edge in worker.graph._edges:\n",
    "    link_data.append({'source': node_id_index[edge._node1._id], \n",
    "                      'target': node_id_index[edge._node2._id]})\n",
    "    \n",
    "xs = LinearScale(min = worker.graph.extreme_cor1['min'], max = worker.graph.extreme_cor1['max'])\n",
    "ys = LinearScale(min = worker.graph.extreme_cor2['min'], max = worker.graph.extreme_cor2['max'])\n",
    "\n",
    "graph = Graph(node_data=node_data, link_data=link_data, link_type='line',\n",
    "              scales={'x': xs, 'y': ys},\n",
    "              colors=['white'],\n",
    "              x=x, \n",
    "              y=y,\n",
    "              directed=False)\n",
    "\n",
    "graph.hovered_style = {'stroke': 'red'}\n",
    "graph.unhovered_style = {'opacity': '0.4'}\n",
    "graph.selected_style = {'opacity': '1', 'stroke': 'green', 'stroke-width': '2.5'}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Define node-selection listener"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "selected_node1 = None\n",
    "selected_node2 = None\n",
    "def print_event(self, target):\n",
    "    global selected_node1\n",
    "    global selected_node2\n",
    "    selected_node1 = selected_node2\n",
    "    selected_node2 = target['data']['id']\n",
    "\n",
    "graph.on_element_click(print_event)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Display the graph and wait for the user to select two nodes by clicking on them"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "fc4a6cadd1c44468b05f30fbe434f5ac",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Figure(fig_margin={'top': 60, 'bottom': 60, 'left': 60, 'right': 60}, layout=Layout(height='600px', width='100…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "73936996f8234b2295cf336b562255a3",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Toolbar(figure=Figure(fig_margin={'top': 60, 'bottom': 60, 'left': 60, 'right': 60}, layout=Layout(height='600…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "f = Figure(marks=[graph], layout=fig_layout)\n",
    "t = Toolbar(figure=f)\n",
    "display(f, t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Find the best path connecting the two selected points and visualize it"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Main ACO loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Nodes were not selected. Generic nodes will be set instead.\n"
     ]
    }
   ],
   "source": [
    "if (selected_node1 == None or selected_node2 == None):\n",
    "    print(\"Nodes were not selected. Generic nodes will be set instead.\")\n",
    "    worker.set_ids(\"3653296222\", \"3653134376\")\n",
    "else:\n",
    "    worker.set_ids(selected_node1, selected_node2)\n",
    "\n",
    "for i in range(worker.iter_max):\n",
    "    worker.create_colony()\n",
    "    successful_ants = worker.searchGraph()\n",
    "    worker.return_weights()\n",
    "    worker.update_weights(successful_ants)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Visualize the best path"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Path length:  2327.7328943657376\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "fc4a6cadd1c44468b05f30fbe434f5ac",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Figure(fig_margin={'top': 60, 'bottom': 60, 'left': 60, 'right': 60}, layout=Layout(height='600px', width='100…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "73936996f8234b2295cf336b562255a3",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Toolbar(figure=Figure(fig_margin={'top': 60, 'bottom': 60, 'left': 60, 'right': 60}, layout=Layout(height='600…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "colors = ['white'] * len(graph.node_data)\n",
    "for edge in worker.best_path:\n",
    "    node1_index = node_id_index[edge._node1._id]\n",
    "    node2_index = node_id_index[edge._node2._id]\n",
    "    colors[node1_index], colors[node2_index] = 'red', 'red'\n",
    "graph.colors = colors\n",
    "\n",
    "print(\"Path length: \", worker.best_path_len)\n",
    "display(f, t)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

class Ant:
    def __init__(self, node):
        self.path_covered = []
        self.path_length = 0
        self.current_node = node

    def uniqify(self):
        covered = {}
        for item in self.path_covered:
            if item in covered: continue
            covered[item] = True
        self.path_covered = list(covered.keys())
from worker import Worker

def main():
    rob = Worker(10, 100)
    rob.run()
    print("Best path distance: ",  rob.best_path_len)
    print("Number of nodes in best path: ", len(rob.best_path))

    """
        Nema garancije da je rob.best_path bez ciklusa
        Ono sto sam primetio je da u 90 posto slucajeva je da je:
        
            len(set(rob.best_path)) == len(rob.best_path)
        
        cak kada i ovo ne vazi razlika bude mozda u 10 cvorova.
        Postoje algoritmi koji uklanjaju cikluse iz puteva u grafu. Ako hoces mozemo primeniti jedan takav
        algoritam kad aco zavrsi poslednju iteraciju, cisto da bi dodatno poboljsali resenje.
        
        Dodao sam nekoliko stvari:
        
        A)
        Promenio sam ucitavanje grafa, ovako je mnogo jednostavnije, mozes pogledati.
        
        
        B)
        Iskomentarisao sam atribut self.weight u Edge-u, posto sam kontao da pri odredjivanju najprivalcnije grane
        treba da utice samo edge._pheromone, a da se edge._distance koristi samo pri odredjivanju koliko ce se pheromone-a
        dodati na granu. Napravio sam sledecu politiku osvezavanja feromona na grani
        
            1) Grane koje ne pripadaju NIJEDNOJ pobednickoj putanji gube feromon po formuli:
                
                self.pheromone *= (1 - evap_fac)
                
            2) Grane koje se nalaze na NAJBOLJOJ pobednickoj putanji dobijaju feromon po formuli:
                
                self.pheromone += pheromon_multiplier / path_distance
                
                gde je pheromone_multiplier konstanta koju sam cisto eksperimentalno odredito u main-u, mozes probati da je stelujes 
                a path_distance ukupna duzina celokupnog puta koji je mrav presao od pocetnog do krajnjeg cvora
                taj path_distance je atribut svakog ant-a, tako da moze da se inkrementalno uvecava svakim njegovim pomeranjem sa
                jedne grane na drugu(ne moram posebno da iteriram kad mrav zavrsi svoju iteraciju).
                
                U metodi _update_pheromone u Edge-u na liniji 67 mozes da odkomentarises pa da umesto pheromon_multiplier / path_distance
                dodajes neku fiksnu konstantu. Ja isprobao oba i zakljucio da oba daju otprilike podjednako dobra najbolja resenja,
                dok su najgora resenja slucaja u koje na pheromone dodajem pheromon_multiplier / path_distance ne veca od 3500 - 3800,
                a u slucaju dodavanja konstante ode i na 4500.
            
            3) Za grane koje se nalaze u pobednickim putanjama, ali ne u najboljoj pobdenickoj putanji, radim i 1) i 2). 
                Iskreno nisam testirao kako radi ako te grane samo iskuliram,
                al moguce da bi onda prerano iskonvergirao jer zanemarujem sva resenja osim najboljeg u svakoj iteraciji.
            
            Bitna stvar je da svakoj grani uvecavam feromon najvise jednom. To sam uradio tako sto sam uveo set() u metodi
            update_weights u worker-u. Bez tog set-a najbolja resenja opet ostanu u slicnom rasponu, ali za najgora resenja ode i na
            5000.
            
        C) 
        Uveo sam lokalnu smanjivanje pheromone-a i politiku da se mrav koji se nalazni na cvoru koji ima vise od jedne grane
        ne moze vratiti na svoj prethodni cvor(ne moze koristiti istu granu iz koje je dosao).Bez ovog resenja odu na 8 - 9 hiljada
        
        E sad ovo lokalno smanjivanje feromona sam uveo da bi se u jednoj iteraciji mravi sto vise rasirili po mogucim granama.
        Svaki put kada mrav izabere granu toj grani se smanji feromon za udeo koji je definisan kao 1 - self.local_evap_fac u workeru.
        Uveo sam i recnik self.edge_stats koji pamti koliko je svakoj grani uzeto na ovaj nacin feromona, i onda,
        kada svi mravi zavrse iteraciju, u metodi return_weights vratim svakoj grani oduzet feromon. Ovo sam kontao da ce ubrzati dosta
        ali se za sad nije tako pokazalo.Mozes testirati bez ove funkcionalnosti takosto ces iskomentarisati liniju 206 i 
        linije od 122 do 127 u workeru. 
            
    """

if __name__ == '__main__':
    main()